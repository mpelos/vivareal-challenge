# README

It's highly recommended you to use docker for development purpose. If you don't have docker installed see: https://docs.docker.com/engine/installation.

## Configuration

```
docker-compose build
docker-compose run app bundle install --with development test
docker-compose run app bundle exec rails db:setup
```

Have patience, the database setup will import thousands of properties.

## Running the Application

```
docker-compose up
```

## Running test suite

```
docker-compose run -e "RAILS_ENV=test" app bundle exec rspec
```

or just leaves a shell opened to run the tests whenever you want:

```
docker-compose run -e "RAILS_ENV=test" app /bin/bash

$ root@xxxxxxxxxxxx:/app/orquestrador# rspec
```

## Running the integration tests

Integration tests are slow by nature, for that reason they are ignored in default `rspec` command. To run the integration tests run:

```
docker-compose run -e "RAILS_ENV=test" app bundle exec rspec -t integration
```
