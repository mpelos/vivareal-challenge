FROM mpelos/rails-node:5.1.2

MAINTAINER Marcelo Pélos

WORKDIR /app

COPY . /app

ENV RAILS_ENV production
ENV RACK_ENV production
ENV SECRET_KEY_BASE $(bundle exec rails secret)

RUN bundle install --jobs 20 --retry 5 --without development test

EXPOSE 3000

CMD ["bundle", "exec", "rails", "server", "-p", "3000", "-b", "0.0.0.0"]
