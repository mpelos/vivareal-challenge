require "net/http"

class PropertyImporter

  class ImportError < StandardError; end

  attr_accessor :json_url

  def initialize(json_url: "https://raw.githubusercontent.com/VivaReal/code-challenge/master/properties.json")
    @json_url = json_url
  end

  def import!(&block)
    uri = URI(json_url)
    response = Net::HTTP.get(uri)
    properties_data = JSON.parse(response)
    property_counter = 0

    properties_data.fetch("properties").each do |property_data|
      begin
        Property.create!(
          title: property_data["title"],
          price: property_data["price"],
          description: property_data["description"],
          x: property_data["lat"],
          y: property_data["long"],
          beds: property_data["beds"],
          baths: property_data["baths"],
          square_meters: property_data["squareMeters"]
        )
      rescue => e
        raise ImportError, "Error on import after the creation of #{property_counter} properties." +
          " property_data: #{property_data}"
      end

      property_counter += 1
      block.call(property_counter) if block_given?
    end

    property_counter
  end
end
