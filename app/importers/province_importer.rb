require "net/http"

class ProvinceImporter

  class ImportError < StandardError; end

  attr_accessor :json_url

  def initialize(json_url: "https://raw.githubusercontent.com/VivaReal/code-challenge/master/provinces.json")
    @json_url = json_url
  end

  def import!
    uri = URI(json_url)
    response = Net::HTTP.get(uri)
    provinces_data = JSON.parse(response)
    province_counter = 0

    provinces_data.each do |province_name, province_data|
      boundaries = province_data.fetch("boundaries")

      begin
        Province.create!(
          name: province_name,
          upper_left_x: boundaries.fetch("upperLeft").fetch("x"),
          upper_left_y: boundaries.fetch("upperLeft").fetch("y"),
          bottom_right_x: boundaries.fetch("bottomRight").fetch("x"),
          bottom_right_y: boundaries.fetch("bottomRight").fetch("y")
        )
      rescue => e
        raise ImportError, "Error on import after the creation of #{province_counter} provinces." +
          " province_data: #{province_data}"
      end

      province_counter += 1
    end

    province_counter
  end
end
