class PropertiesController < ApplicationController
  # GET /properties
  def index
    ax, ay, bx, by = params[:ax], params[:ay], params[:bx], params[:by]

    if params[:ax] && params[:ay] && params[:bx] && params[:by]
      @properties = Property.find_all_in_area(ax, ay, bx, by)
      render :index
    else
      render json: { required_parameters: ["ax", "ay", "bx", "by"] }, status: :bad_request
    end
  end

  # GET /properties/1
  def show
    @property = Property.find(params[:id])
    render :show
  rescue ActiveRecord::RecordNotFound
    render json: { error: "Not Found" }, status: :not_found
  end

  # POST /properties
  def create
    @property = Property.new(property_params)

    if @property.save
      render :show, status: :created, location: @property
    else
      render json: { error: @property.errors }, status: :unprocessable_entity
    end
  end

  private

  # Only allow a trusted parameter "white list" through.
  def property_params
    params.permit(:title, :price, :description, :x, :y, :beds, :baths, :squareMeters)
  end
end
