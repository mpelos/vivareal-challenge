class Province < ApplicationRecord
  validates :name, presence: true, uniqueness: true
  validates :upper_left_x, presence: true
  validates :upper_left_y, presence: true
  validates :bottom_right_x, presence: true
  validates :bottom_right_y, presence: true

  before_update :expire_properties_cache

  scope :find_from_point, ->(x, y) do
    where(
      "upper_left_x <= :x AND upper_left_y >= :y AND bottom_right_x >= :x AND bottom_right_y <= :y",
      x: x, y: y
    )
  end

  private

  def expire_properties_cache
    name = name_changed? ? name_was : self.name
    Property.find_all_from_province(name).expire_cache
  end
end
