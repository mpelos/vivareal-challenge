class Property < ApplicationRecord
  validates :title, presence: true
  validates :price, presence: true
  validates :description, presence: true
  validates :x, presence: true, numericality: { greater_than_or_equal_to: 0, less_than_or_equal_to: 1400 }
  validates :y, presence: true, numericality: { greater_than_or_equal_to: 0, less_than_or_equal_to: 1000 }
  validates :beds, presence: true, numericality: { greater_than: 0, less_than_or_equal_to: 5 }
  validates :baths, presence: true, numericality: { greater_than: 0, less_than_or_equal_to: 4 }
  validates :square_meters, presence: true, numericality: { greater_than_or_equal_to: 20, less_than_or_equal_to: 240 }

  scope :find_all_from_province, ->(province_name) { where("? = ANY (provinces_cache)", province_name) }
  scope :find_all_in_area, -> (ax, ay, bx, by) { where("x >= ? AND y <= ? AND x <= ? AND y >= ?", ax, ay, bx, by) }
  scope :expire_cache, -> { update_all provinces_cache: [] }

  def squareMeters
    self[:square_meters]
  end

  def squareMeters=(value)
    self[:square_meters] = value
  end

  def provinces
    return self[:provinces_cache] if self[:provinces_cache].present?

    provinces_names = Province.find_from_point(x, y).map(&:name)
    update_attribute :provinces_cache, provinces_names if persisted?

    provinces_names
  end
end
