json.foundProperties @properties.size

json.properties @properties do |property|
  json.id property.id
  json.title property.title
  json.price property.price
  json.description property.description
  json.x property.x
  json.y property.y
  json.beds property.beds
  json.baths property.baths
  json.squareMeters property.square_meters
  json.provinces property.provinces
end
