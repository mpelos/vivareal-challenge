Rails.application.routes.draw do
  resources :properties, defaults: { format: :json }, only: [:index, :create, :show]

  match '/', via: :all, to: proc { [404, {}, ['']] }
end
