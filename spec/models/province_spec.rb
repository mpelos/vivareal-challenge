require "rails_helper"

RSpec.describe Province, type: :model do
  it { is_expected.to validate_presence_of :name }
  it { is_expected.to validate_presence_of :upper_left_x }
  it { is_expected.to validate_presence_of :upper_left_y }
  it { is_expected.to validate_presence_of :bottom_right_x }
  it { is_expected.to validate_presence_of :bottom_right_y }

  def create_helper(name, upper_left_x, upper_left_y, bottom_right_x, bottom_right_y)
    Province.create!(
      name: name,
      upper_left_x: upper_left_x,
      upper_left_y: upper_left_y,
      bottom_right_x: bottom_right_x,
      bottom_right_y: bottom_right_y
    )
  end

  describe ".find_from_point" do
    subject { described_class.find_from_point(x, y) }

    let(:upper_left_province) { create_helper("upper_left_province", 0, 1000, 500, 600) }
    let(:bottom_left_province) { create_helper("bottom_left_province", 0, 400, 500, 0) }
    let(:upper_right_province) { create_helper("upper_right_province", 900, 1000, 1400, 600) }
    let(:bottom_right_province) { create_helper("bottom_right_province", 900, 400, 1400, 0) }
    let(:center_province) { create_helper("center_province", 400, 700, 1000, 300) }

    context "(0, 0)" do
      let(:x) { 0 }
      let(:y) { 0 }

      it { is_expected.to eq [bottom_left_province] }
    end

    context "(0, 1000)" do
      let(:x) { 0 }
      let(:y) { 1000 }

      it { is_expected.to eq [upper_left_province] }
    end

    context "(1400, 1000)" do
      let(:x) { 1400 }
      let(:y) { 1000 }

      it { is_expected.to eq [upper_right_province] }
    end

    context "(0, 0)" do
      let(:x) { 1400 }
      let(:y) { 0 }

      it { is_expected.to eq [bottom_right_province] }
    end

    context "(700, 800)" do
      let(:x) { 700 }
      let(:y) { 800 }

      it { is_expected.to be_empty }
    end

    context "(1150, 800)" do
      let(:x) { 1150 }
      let(:y) { 800 }

      it { is_expected.to eq [upper_right_province] }
    end

    context "(1150, 500)" do
      let(:x) { 1150 }
      let(:y) { 500 }

      it { is_expected.to be_empty }
    end

    context "(1150, 200)" do
      let(:x) { 1150 }
      let(:y) { 200 }

      it { is_expected.to eq [bottom_right_province] }
    end

    context "(700, 200)" do
      let(:x) { 700 }
      let(:y) { 200 }

      it { is_expected.to be_empty }
    end

    context "(250, 200)" do
      let(:x) { 250 }
      let(:y) { 200 }

      it { is_expected.to eq [bottom_left_province] }
    end

    context "(250, 500)" do
      let(:x) { 250 }
      let(:y) { 500 }

      it { is_expected.to be_empty }
    end

    context "(250, 800)" do
      let(:x) { 250 }
      let(:y) { 800 }

      it { is_expected.to eq [upper_left_province] }
    end

    context "(700, 500)" do
      let(:x) { 700 }
      let(:y) { 500 }

      it { is_expected.to eq [center_province] }
    end

    context "(950, 650)" do
      let(:x) { 950 }
      let(:y) { 650 }

      it { is_expected.to eq [upper_right_province, center_province] }
    end

    context "(950, 350)" do
      let(:x) { 950 }
      let(:y) { 350 }

      it { is_expected.to eq [bottom_right_province, center_province] }
    end

    context "(450, 350)" do
      let(:x) { 450 }
      let(:y) { 350 }

      it { is_expected.to eq [bottom_left_province, center_province] }
    end

    context "(450, 650)" do
      let(:x) { 450 }
      let(:y) { 650 }

      it { is_expected.to eq [upper_left_province, center_province] }
    end
  end
end
