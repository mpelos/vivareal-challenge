require "rails_helper"

RSpec.describe Property, type: :model do
  subject(:model) do
    Property.new(
      title: title,
      price: price,
      description: description,
      x: x,
      y: y,
      beds: beds,
      baths: baths,
      square_meters: square_meters,
    )
  end

  let(:title) { Faker::Lorem.sentence }
  let(:price) { Faker::Number.between(100_000, 10_000_000) }
  let(:description) { Faker::Lorem.paragraph }
  let(:x) { Faker::Number.between(0, 1400) }
  let(:y) { Faker::Number.between(0, 1000) }
  let(:beds) { Faker::Number.between(1, 5) }
  let(:baths) { Faker::Number.between(1, 4) }
  let(:square_meters) { Faker::Number.between(20, 240) }

  it { is_expected.to validate_presence_of :title }
  it { is_expected.to validate_presence_of :price }
  it { is_expected.to validate_presence_of :description }
  it { is_expected.to validate_presence_of :x }
  it { is_expected.to validate_numericality_of(:x).is_greater_than_or_equal_to(0).is_less_than_or_equal_to(1400) }
  it { is_expected.to validate_presence_of :y }
  it { is_expected.to validate_numericality_of(:y).is_greater_than_or_equal_to(0).is_less_than_or_equal_to(1000) }
  it { is_expected.to validate_presence_of :beds }
  it { is_expected.to validate_numericality_of(:beds).is_greater_than(0).is_less_than_or_equal_to(5) }
  it { is_expected.to validate_presence_of :baths }
  it { is_expected.to validate_numericality_of(:baths).is_greater_than(0).is_less_than_or_equal_to(4) }
  it { is_expected.to validate_presence_of :square_meters }
  it do
    is_expected
      .to validate_numericality_of(:square_meters)
      .is_greater_than_or_equal_to(20)
      .is_less_than_or_equal_to(240)
  end

  describe ".find_all_from_province" do
    subject { described_class.find_all_from_province(province_name) }

    let(:x) { Faker::Number.between(0, 1390) }

    let!(:province) do
      Province.create!(
        name: Faker::Lorem.word,
        upper_left_x: 0,
        upper_left_y: 1000,
        bottom_right_x: 1390,
        bottom_right_y: 0
      )
    end

    before do
      model.provinces_cache = ["whatever-province", province.name]
      model.save!
    end

    context "province exists" do
      let(:province_name) { province.name }

      it { is_expected.to eq [model] }
    end

    context "no properties in province" do
      let(:province_name) { isolated_province.name }

      let!(:isolated_province) do
        Province.create!(
          name: "where-judas-lost-his-boots",
          upper_left_x: 1391,
          upper_left_y: 10,
          bottom_right_x: 1400,
          bottom_right_y: 0
        )
      end

      it { is_expected.to be_empty }
    end
  end

  describe ".find_all_from_province" do
    subject { described_class.find_all_in_area ax, ay, bx, by }

    let(:ax) { 200 }
    let(:ay) { 800 }
    let(:bx) { 1200 }
    let(:by) { 200 }

    let!(:property_in_area) do
      model.dup.tap do |property|
        property.x = 700
        property.y = 500
        property.save
      end
    end

    let!(:property_out_of_area) do
      model.dup.tap do |property|
        property.x = 100
        property.y = 100
        property.save
      end
    end

    it { is_expected.to eq [property_in_area] }
  end

  describe "#provinces" do
    subject { model.provinces }

    let!(:upper_left_province) do
      Province.create!(
        name: "upper_left_province",
        upper_left_x: 0,
        upper_left_y: 1000,
        bottom_right_x: 1050,
        bottom_right_y: 250
      )
    end

    let!(:bottom_right_province) do
      Province.create!(
        name: "bottom_right_province",
        upper_left_x: 350,
        upper_left_y: 750,
        bottom_right_x: 1400,
        bottom_right_y: 0
      )
    end

    context "outland" do
      let(:x) { 100 }
      let(:y) { 100 }

      it { is_expected.to be_empty }
    end

    context "one province" do
      let(:x) { 100 }
      let(:y) { 900 }

      it { is_expected.to eq ["upper_left_province"] }
    end

    context "intersection of two provinces" do
      let(:x) { 700 }
      let(:y) { 500 }

      it { is_expected.to eq ["upper_left_province", "bottom_right_province"] }
    end

    context "with persisted record" do
      before do
        model.save
      end

      it "updates the provinces_cache attribute" do
        provinces = subject
        expect(model.provinces_cache).to eq provinces
      end
    end

    context "with new record" do
      it "doesn't update the provinces_cache attribute" do
        subject
        expect(model.provinces_cache).to eq []
      end
    end
  end
end
