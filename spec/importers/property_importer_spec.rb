require "rails_helper"

RSpec.describe PropertyImporter do
  subject(:importer) { described_class.new json_url: json_url }

  let(:json_url) { "http://le-url.com" }

  its(:json_url) { is_expected.to eq json_url }

  describe "#import!" do
    subject { importer.import! }

    let(:properties_data) do
      {
        "properties" => [
          {
            "title" => "property_1", "price" => 100_000, "description" => "le-description",
            "lat" => 0, "long" => 0, "beds" => 1, "baths" => 1, "squareMeters" => 60
          }, {
            "title" => "property_2", "price" => 200_000, "description" => "le-description",
            "lat" => 0, "long" => 0, "beds" => 2, "baths" => 2, "squareMeters" => 80
          }
        ]
      }
    end

    before do
      stub_request(:get, json_url).to_return(body: properties_data.to_json)
      allow(Property).to receive(:create!)
    end

    it "shows the number of records created" do
      is_expected.to eq 2
    end

    it "creates properties mapping the attributes correctly" do
      property_data = properties_data["properties"].first

      expect(Property).to receive(:create!).with(
        title: property_data.fetch("title"),
        price: property_data.fetch("price"),
        description: property_data.fetch("description"),
        x: property_data.fetch("lat"),
        y: property_data.fetch("long"),
        beds: property_data.fetch("beds"),
        baths: property_data.fetch("baths"),
        square_meters: property_data.fetch("squareMeters")
      )

      subject
    end

    context "when a property is invalid" do
      before do
        allow(Property).to receive(:create!).and_raise(StandardError)
      end

      it { expect { subject }.to raise_error(described_class::ImportError) }
    end
  end
end
