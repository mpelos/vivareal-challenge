require "rails_helper"

RSpec.describe ProvinceImporter do
  subject(:importer) { described_class.new json_url: json_url }

  let(:json_url) { "http://le-url.com" }

  its(:json_url) { is_expected.to eq json_url }

  describe "#import!" do
    subject { importer.import! }

    let(:province_1_data) do
      {
        "name" => Faker::Lorem.word,
        "upper_left_x" => Faker::Number.between(0, 1400),
        "upper_left_y" => Faker::Number.between(0, 1000),
        "bottom_right_x" => Faker::Number.between(0, 1400),
        "bottom_right_y" => Faker::Number.between(0, 1000)
      }
    end

    let(:province_2_data) do
      {
        "name" => Faker::Lorem.word,
        "upper_left_x" => Faker::Number.between(0, 1400),
        "upper_left_y" => Faker::Number.between(0, 1000),
        "bottom_right_x" => Faker::Number.between(0, 1400),
        "bottom_right_y" => Faker::Number.between(0, 1000)
      }
    end

    let(:provinces_data) do
      {
        province_1_data["name"] => {
          "boundaries" => {
            "upperLeft" => { "x" => province_1_data["upper_left_x"], "y" => province_1_data["upper_left_y"] },
            "bottomRight" => { "x" => province_1_data["bottom_right_x"], "y" => province_1_data["bottom_right_y"] }
          }
        },
        province_2_data["name"] => {
          "boundaries" => {
            "upperLeft" => { "x" => province_2_data["upper_left_x"], "y" => province_2_data["upper_left_y"] },
            "bottomRight" => { "x" => province_2_data["bottom_right_x"], "y" => province_2_data["bottom_right_y"] }
          }
        }
      }
    end

    before do
      stub_request(:get, json_url).to_return(body: provinces_data.to_json)
      allow(Province).to receive(:create!)
    end

    it "shows the number of records created" do
      is_expected.to eq 2
    end

    it "creates provinces mapping the attributes correctly" do
      expect(Province).to receive(:create!).with(
        name: province_1_data.fetch("name"),
        upper_left_x: province_1_data.fetch("upper_left_x"),
        upper_left_y: province_1_data.fetch("upper_left_y"),
        bottom_right_x: province_1_data.fetch("bottom_right_x"),
        bottom_right_y: province_1_data.fetch("bottom_right_y")
      )

      subject
    end

    context "when a province is invalid" do
      before do
        allow(Province).to receive(:create!).and_raise(StandardError)
      end

      it { expect { subject }.to raise_error(described_class::ImportError) }
    end
  end
end
