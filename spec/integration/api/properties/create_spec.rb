require "rails_helper"

RSpec.describe "POST /properties", :integration do
  subject { response }

  before do
    province
    post "/properties", params: params
  end

  let(:title) { Faker::Lorem.sentence }
  let(:price) { Faker::Number.between(100_000, 10_000_000) }
  let(:description) { Faker::Lorem.paragraph }
  let(:x) { Faker::Number.between(0, 1400) }
  let(:y) { Faker::Number.between(0, 1000) }
  let(:beds) { Faker::Number.between(1, 5) }
  let(:baths) { Faker::Number.between(1, 4) }
  let(:square_meters) { Faker::Number.between(20, 240) }

  let(:params) do
    {
      title: title,
      price: price,
      description: description,
      x: x,
      y: y,
      beds: beds,
      baths: baths,
      squareMeters: square_meters
    }.compact
  end

  let(:parsed_body) { JSON.parse(response.body) }

  let(:province) do
    Province.create!(
      name: Faker::Lorem.word,
      upper_left_x: 0,
      upper_left_y: 1000,
      bottom_right_x: 1400,
      bottom_right_y: 0
    )
  end

  context "valid property" do
    its(:status) { is_expected.to be 201 }
    its(:location) { is_expected.to eq "#{response.request.url}/#{parsed_body['id']}" }

    describe "body" do
      subject { parsed_body }

      its(["id"]) { is_expected.to be_a Integer }
      its(["title"]) { is_expected.to eq title }
      its(["price"]) { is_expected.to eq price }
      its(["description"]) { is_expected.to eq description }
      its(["x"]) { is_expected.to eq x }
      its(["y"]) { is_expected.to eq y }
      its(["beds"]) { is_expected.to eq beds }
      its(["baths"]) { is_expected.to eq baths }
      its(["provinces"]) { is_expected.to eq [province.name] }
      its(["squareMeters"]) { is_expected.to eq square_meters }
    end
  end

  context "when title is absent" do
    let(:title) { nil }

    its(:status) { is_expected.to be 422 }
    it { expect(parsed_body["error"].keys).to eq ["title"] }
  end

  context "when price is absent" do
    let(:price) { nil }

    its(:status) { is_expected.to be 422 }
    it { expect(parsed_body["error"].keys).to eq ["price"] }
  end

  context "when description is absent" do
    let(:description) { nil }

    its(:status) { is_expected.to be 422 }
    it { expect(parsed_body["error"].keys).to eq ["description"] }
  end

  context "when x is absent" do
    let(:x) { nil }

    its(:status) { is_expected.to be 422 }
    it { expect(parsed_body["error"].keys).to eq ["x"] }
  end

  context "when x is lesser than 0" do
    let(:x) { -1 }

    its(:status) { is_expected.to be 422 }
    it { expect(parsed_body["error"].keys).to eq ["x"] }
  end

  context "when x is greater than 1400" do
    let(:x) { 1401 }

    its(:status) { is_expected.to be 422 }
    it { expect(parsed_body["error"].keys).to eq ["x"] }
  end

  context "when y is absent" do
    let(:y) { nil }

    its(:status) { is_expected.to be 422 }
    it { expect(parsed_body["error"].keys).to eq ["y"] }
  end

  context "when y is lesser than 0" do
    let(:y) { -1 }

    its(:status) { is_expected.to be 422 }
    it { expect(parsed_body["error"].keys).to eq ["y"] }
  end

  context "when y is greater than 1000" do
    let(:y) { 1001 }

    its(:status) { is_expected.to be 422 }
    it { expect(parsed_body["error"].keys).to eq ["y"] }
  end

  context "when beds is absent" do
    let(:beds) { nil }

    its(:status) { is_expected.to be 422 }
    it { expect(parsed_body["error"].keys).to eq ["beds"] }
  end

  context "when beds is lesser than 1" do
    let(:beds) { 0 }

    its(:status) { is_expected.to be 422 }
    it { expect(parsed_body["error"].keys).to eq ["beds"] }
  end

  context "when beds is greater than 5" do
    let(:beds) { 6 }

    its(:status) { is_expected.to be 422 }
    it { expect(parsed_body["error"].keys).to eq ["beds"] }
  end

  context "when baths is absent" do
    let(:baths) { nil }

    its(:status) { is_expected.to be 422 }
    it { expect(parsed_body["error"].keys).to eq ["baths"] }
  end

  context "when baths is lesser than 1" do
    let(:baths) { 0 }

    its(:status) { is_expected.to be 422 }
    it { expect(parsed_body["error"].keys).to eq ["baths"] }
  end

  context "when baths is greater than 4" do
    let(:baths) { 5 }

    its(:status) { is_expected.to be 422 }
    it { expect(parsed_body["error"].keys).to eq ["baths"] }
  end

  context "when squareMeters is absent" do
    let(:square_meters) { nil }

    its(:status) { is_expected.to be 422 }
    it { expect(parsed_body["error"].keys).to eq ["square_meters"] }
  end

  context "when squareMeters is lesser than 20" do
    let(:square_meters) { 19 }

    its(:status) { is_expected.to be 422 }
    it { expect(parsed_body["error"].keys).to eq ["square_meters"] }
  end

  context "when squareMeters is greater than 240" do
    let(:square_meters) { 241 }

    its(:status) { is_expected.to be 422 }
    it { expect(parsed_body["error"].keys).to eq ["square_meters"] }
  end
end
