require "rails_helper"

RSpec.describe "GET /properties/{id}", :integration do
  subject { response }

  let(:id) { property.id }

  before do
    property
    province
    get "/properties/#{id}"
  end

  let(:property) do
    Property.create!(
      title: Faker::Lorem.sentence,
      price: Faker::Number.between(100_000, 10_000_000),
      description: Faker::Lorem.paragraph,
      x: Faker::Number.between(0, 1400),
      y: Faker::Number.between(0, 1000),
      beds: Faker::Number.between(1, 5),
      baths: Faker::Number.between(1, 4),
      square_meters: Faker::Number.between(20, 240)
    )
  end

  let(:province) do
    Province.create!(
      name: Faker::Lorem.word,
      upper_left_x: 0,
      upper_left_y: 1000,
      bottom_right_x: 1400,
      bottom_right_y: 0
    )
  end

  let(:parsed_body) { JSON.parse(response.body) }

  context "valid property" do
    its(:status) { is_expected.to be 200 }

    describe "body" do
      subject { parsed_body }

      its(["id"]) { is_expected.to eq property.id }
      its(["title"]) { is_expected.to eq property.title }
      its(["price"]) { is_expected.to eq property.price }
      its(["description"]) { is_expected.to eq property.description }
      its(["x"]) { is_expected.to eq property.x }
      its(["y"]) { is_expected.to eq property.y }
      its(["beds"]) { is_expected.to eq property.beds }
      its(["baths"]) { is_expected.to eq property.baths }
      its(["provinces"]) { is_expected.to eq [province.name] }
      its(["squareMeters"]) { is_expected.to eq property.square_meters }
    end
  end

  context "property does not exist" do
    let(:id) { property.id + 100 }

    its(:status) { is_expected.to be 404 }
  end
end
