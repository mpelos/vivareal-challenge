require "rails_helper"

RSpec.describe "GET /properties/{id}", :integration do
  subject { response }

  let(:id) { property.id }

  before do
    property_in_area
    property_out_of_area
    uri = URI("/properties")
    uri.query = URI.encode_www_form(params)
    get uri
  end

  let(:params) { { ax: ax, ay: ay, bx: bx, by: by }.compact }
  let(:ax) { 200 }
  let(:ay) { 800 }
  let(:bx) { 1200 }
  let(:by) { 200 }

  let!(:property_in_area) do
    Property.create!(
      title: Faker::Lorem.sentence,
      price: Faker::Number.between(100_000, 10_000_000),
      description: Faker::Lorem.paragraph,
      x: 700,
      y: 500,
      beds: Faker::Number.between(1, 5),
      baths: Faker::Number.between(1, 4),
      square_meters: Faker::Number.between(20, 240)
    )
  end

  let!(:property_out_of_area) do
    Property.create!(
      title: Faker::Lorem.sentence,
      price: Faker::Number.between(100_000, 10_000_000),
      description: Faker::Lorem.paragraph,
      x: 1400,
      y: 1000,
      beds: Faker::Number.between(1, 5),
      baths: Faker::Number.between(1, 4),
      square_meters: Faker::Number.between(20, 240)
    )
  end

  let(:parsed_body) { JSON.parse(response.body) }

  its(:status) { is_expected.to be 200 }

  describe "body" do
    subject { parsed_body }

    its(["foundProperties"]) { is_expected.to eq 1 }
    it { expect(parsed_body["properties"].size).to eq 1 }

    describe "first property" do
      subject { parsed_body["properties"].first }

      its(["id"]) { is_expected.to eq property_in_area.id }
      its(["title"]) { is_expected.to eq property_in_area.title }
      its(["price"]) { is_expected.to eq property_in_area.price }
      its(["description"]) { is_expected.to eq property_in_area.description }
      its(["x"]) { is_expected.to eq property_in_area.x }
      its(["y"]) { is_expected.to eq property_in_area.y }
      its(["beds"]) { is_expected.to eq property_in_area.beds }
      its(["baths"]) { is_expected.to eq property_in_area.baths }
      its(["provinces"]) { is_expected.to eq [] }
      its(["squareMeters"]) { is_expected.to eq property_in_area.square_meters }
    end
  end

  context "missing 'ax' parameter" do
    let(:ax) { nil }

    its(:status) { is_expected.to be 400 }
  end

  context "missing 'ay' parameter" do
    let(:ay) { nil }

    its(:status) { is_expected.to be 400 }
  end

  context "missing 'bx' parameter" do
    let(:bx) { nil }

    its(:status) { is_expected.to be 400 }
  end

  context "missing 'by' parameter" do
    let(:by) { nil }

    its(:status) { is_expected.to be 400 }
  end
end
