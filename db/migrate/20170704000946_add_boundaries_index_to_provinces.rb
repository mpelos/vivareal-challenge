class AddBoundariesIndexToProvinces < ActiveRecord::Migration[5.1]
  def change
    add_index :provinces,
              [:upper_left_x, :upper_left_y, :bottom_right_x, :bottom_right_y],
              name: :index_provinces_on_boundaries
  end
end
