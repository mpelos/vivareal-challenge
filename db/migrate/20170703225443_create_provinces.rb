class CreateProvinces < ActiveRecord::Migration[5.1]
  def change
    create_table :provinces do |t|
      t.string :name, null: false
      t.integer :upper_left_x, null: false
      t.integer :upper_left_y, null: false
      t.integer :bottom_right_x, null: false
      t.integer :bottom_right_y, null: false

      t.index :name, unique: true

      t.timestamps
    end
  end
end
