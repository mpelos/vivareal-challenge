class AddProvincesCacheToProperties < ActiveRecord::Migration[5.1]
  def change
    add_column :properties, :provinces_cache, :string, array: true, default: [], null: false
    add_index :properties, :provinces_cache, using: :gin
  end
end
