class CreateProperties < ActiveRecord::Migration[5.1]
  def change
    create_table :properties do |t|
      t.string :title, null: false
      t.integer :price, null: false
      t.text :description, null: false
      t.integer :x, null: false
      t.integer :y, null: false
      t.integer :beds, null: false
      t.integer :baths, null: false
      t.integer :square_meters, null: false

      t.timestamps
    end
  end
end
