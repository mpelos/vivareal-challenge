puts "Importing provinces..."
provinces_count = ProvinceImporter.new.import!
puts "Total of #{provinces_count} provinces were imported."

puts "\nImporting properties..."
properties_count = PropertyImporter.new.import! do |length|
  puts "#{length} properties were created" if length % 500 == 0
end
puts "Total of #{properties_count} properties were imported."
